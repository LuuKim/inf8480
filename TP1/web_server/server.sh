#!/bin/bash
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y apache2
sudo apt-get install -y php7.0
sudo apt-get install -y php7.0-pgsql
sudo apt-get install -y libapache2-mod-php
sudo rm /var/www/html/index.html
sudo cp -r web/* /var/www/html/
sudo service apache2 restart

