<?php
require_once 'identifiants.php'; // fichier des identifiants BDD

$PROFILE_DEV = true;
function connexionDB($adresse) //Fonction permettant de se connecter à une base de donnée
{
    try
    {
        $options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        $BDD = new PDO( 'pgsql:host=' . $adresse .';dbname=inf8480', LOGIN_BDD, PASS_BDD, $options);

    }
    catch (PDOException $e)
    {

        //PROD
        print $e;
        print "Erreur :";
        $BDD =null;
    }

    return $BDD;
}


/*
 * **************************** CATEGORIE *******************************************
 */
function getAllEtudiants(PDO $BDD){
    $request = $BDD->query('SELECT * FROM etudiants;');
    return $request->fetchAll(PDO::FETCH_ASSOC);
}
