<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php'; // fichier des fonctions

if ($_POST['type'] = "bdd" && isset($_POST['adress'])) {
    try {
        $bdd = connexionDB($_POST['adress']);
        if (isset($bdd)) {

            $etudiants = getAllEtudiants($bdd);
            if (!empty($etudiants)) {
                foreach ($etudiants as $etudiant) {
                    echo "Matricule :". $etudiant['matricule'] . "  Nom :" . $etudiant['name'];
                }
            }
        } else {
            echo 'Erreur de connexion à la base de donnée !';
        }
        $bdd = null;
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }


} else {
    echo "Remplissez le champ !";
}