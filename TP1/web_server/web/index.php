<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php'; // fichier des fonctions
session_start();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="bootstrap-4.4.1/css/bootstrap.css">
    <style>
        .loader {
            display: inline-block;
            border-top: 4px solid grey;
            border-right: 4px solid lightblue;
            border-bottom: 4px solid grey;
            border-left: 4px solid lightblue;

            border-radius: 50%;
            width: 15px;
            height: 15px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
</head>
<body>

<div class="container">

    <img src="logo.png" alt="logo" width="200">
    <h1>INF8480 -TP1</h1>

    <div id="base">
        <p>Entrer l'adresse IP interne de l'instance openstack ou est installée la base. </p>
        <form class="form-inline">
            <label class="sr-only" for="adress">Adresse IP : </label>
            <input class="form-control mb-1 mr-sm-1 mb-sm-0" id="adress" type="text" placeholder="10.253.xxx.xxx">
            <button type="submit" id="submit1" class="btn btn-primary">Valider <div class="loader" id="loader" hidden> </button>
        </form>
        <br>

        <div class="alert alert-primary" role="alert" id="resultbox1" hidden>
            <p id="result1"></p>
        </div>
    </div>


<div id="validation" hidden>
    <p>Afin de valider votre TP, compléter ce formulaire avec votre (si vous êtes seul) ou vos deux matricules à 7 chiffres, et cliquer sur valider.
        Le hash obtenu est à recopier dans le moodlequiz du TP. </p>
    <form class="form-inline">
        <label class="sr-only" for="cip1">Nom : </label>
        <input class="form-control mb-1 mr-sm-1 mb-sm-0" id="cip1" type="text" maxlength="7"
               placeholder="Matricule 1">
        <label class="sr-only" for="cip2">Nom : </label>
        <input class="form-control mb-1 mr-sm-1 mb-sm-0" id="cip2" type="text" maxlength="7"
               placeholder="Matricule 2">
        <button type="submit" id="submit" class="btn btn-primary">Valider</button>
    </form>
<br>

    <div class="alert alert-primary" role="alert" id="resultbox" hidden>
        <p id="result"></p>
    </div>
</div>
</div>

<script src="jquery-3.4.1.min.js"></script>
<script src="bootstrap-4.4.1/js/bootstrap.min.js"></script>
<script>
    var _0x3eb5=['validate','#cip2','&cip1=','#resultbox','#result','Remplissez\x20au\x20moins\x20un\x20champ\x20!','POST','validate.php','Une\x20erreur\x20s\x27est\x20produite\x20!','ready','click','bdd','#adress','val','type=','&adress=','removeAttr','hidden','#result1','text','Remplissez\x20le\x20champ\x20!','getElementById','loader','ajax','parse.php','#resultbox1','true','Matricule\x20:10052089\x20\x20Nom\x20:bobby','#validation'];(function(_0x231425,_0x28a31e){var _0x3ac0fc=function(_0x341729){while(--_0x341729){_0x231425['push'](_0x231425['shift']());}};_0x3ac0fc(++_0x28a31e);}(_0x3eb5,0x1bc));var _0x4cf3=function(_0x231425,_0x28a31e){_0x231425=_0x231425-0x0;var _0x3ac0fc=_0x3eb5[_0x231425];return _0x3ac0fc;};$(document)[_0x4cf3('0x0')](function(){var _0x4f5afa=![];$('#submit1')[_0x4cf3('0x1')](function(){var _0x1bda75=_0x4cf3('0x2');var _0x528d38=$(_0x4cf3('0x3'))[_0x4cf3('0x4')]();var _0x387fc5=_0x4cf3('0x5')+_0x1bda75+_0x4cf3('0x6')+_0x528d38;if(_0x1bda75==''||_0x528d38==''){$('#resultbox1')[_0x4cf3('0x7')](_0x4cf3('0x8'));$(_0x4cf3('0x9'))[_0x4cf3('0xa')](_0x4cf3('0xb'));}else{document[_0x4cf3('0xc')](_0x4cf3('0xd'))['removeAttribute'](_0x4cf3('0x8'));$[_0x4cf3('0xe')]({'type':'POST','url':_0x4cf3('0xf'),'data':_0x387fc5,'cache':![],'success':function(_0x427b67){$(_0x4cf3('0x10'))[_0x4cf3('0x7')](_0x4cf3('0x8'));$(_0x4cf3('0x9'))['text'](_0x427b67);document[_0x4cf3('0xc')](_0x4cf3('0xd'))['setAttribute']('hidden',_0x4cf3('0x11'));if(_0x427b67===_0x4cf3('0x12')){_0x4f5afa=!![];$(_0x4cf3('0x13'))[_0x4cf3('0x7')](_0x4cf3('0x8'));}}});}return![];});$('#submit')[_0x4cf3('0x1')](function(){var _0x4790b2=_0x4cf3('0x14');var _0x85c0f6=$('#cip1')[_0x4cf3('0x4')]();var _0x1b2c1c=$(_0x4cf3('0x15'))[_0x4cf3('0x4')]();var _0xc7956=_0x4cf3('0x5')+_0x4790b2+_0x4cf3('0x16')+_0x85c0f6+'&cip2='+_0x1b2c1c;if(_0x4790b2==''||_0x85c0f6==''){$(_0x4cf3('0x17'))[_0x4cf3('0x7')](_0x4cf3('0x8'));$(_0x4cf3('0x18'))[_0x4cf3('0xa')](_0x4cf3('0x19'));}else if(_0x4f5afa===!![]){$[_0x4cf3('0xe')]({'type':_0x4cf3('0x1a'),'url':_0x4cf3('0x1b'),'data':_0xc7956,'cache':![],'success':function(_0x2b5629){$(_0x4cf3('0x17'))[_0x4cf3('0x7')]('hidden');$(_0x4cf3('0x18'))['text'](_0x2b5629);}});}else{$(_0x4cf3('0x17'))[_0x4cf3('0x7')]('hidden');$(_0x4cf3('0x18'))['text'](_0x4cf3('0x1c'));}return![];});});
</script>

</body>
</html>
